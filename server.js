const path = require("path");
const express = require("express");
const app = express(); // create express app

const indexFileLocation = path.join(__dirname, "index.html")

app.get('/*', express.static("./"), (req, res) => {
  res.sendFile(indexFileLocation)
})
app.listen(3000, () => {
  console.log("server started on port 3000");
});
