
import React from 'react'
import converFormImage from '!!file-loader!../../assets/convert-form.png'

const styles = {
  formBanner: {
    width: '100%',
    backgroundColor: '#2A3B57',//'#050527ba',
    height: (window.innerHeight * .3) + 25 + 5
  },
  imageDiv: {
    position: 'absolute',
    marginTop: 60,
    width: window.innerWidth > 760? '68%' :'86%',
    // backgroundColor: '#716464',
    height: window.innerWidth > 760 ? ((window.innerHeight * .4) + 25 + 5) :((window.innerHeight * .3) + 25 + 5),
    borderRadius: 30,
    marginLeft: window.innerWidth > 760? '16%' :'7%',
    marginRight: '7%'
  },
  formBannerMain: {
    height: window.innerWidth > 760 ? ((window.innerHeight * .4) + 25 + 5) :((window.innerHeight * .3) + 25 + 5)
  }
}

export default class Banner extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="form-banner-main" style={styles.formBannerMain}>
        <div className="form-banner" style={styles.formBanner}>
          <div className="banner-image" style={styles.imageDiv}>
            <img src={converFormImage} width='100%' height='100%'style={{borderRadius: 50}}/>
          </div>
        </div>
      </div>
    )
  }

}
