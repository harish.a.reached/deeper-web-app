import React from 'react'
import { withRouter } from 'react-router-dom'
import { history } from '../../history'
import { Row, Col } from 'react-bootstrap'
import { FormControlLabel, Checkbox } from '@material-ui/core'
import axios from 'axios'


import config from '../../config'
import HInput from '../../form-components/HInput'
import Banner from '../Banner'
import HButton from '../../form-components/HButton'
import Dropdown from '../../form-components/Dropdown'
import { FORMS_CONFIG } from '../../constants'
import { getAllLanguages, getAllAgeGroups, getAllCountries } from '../../utils/formUtils'
import Logo from '!!file-loader!../../assets/logo.png'
import PlayStore from '!!file-loader!../../assets/playstore.png'
import HomeIcon from '!!file-loader!../../assets/home-icon.png'
import FileUpload from '../../form-components/FileUpload'

const isMobile = window.outerWidth < 760
const styles = {
  errorMessage: {
    padding: 10,
    marginBottom: 20,
    marginLeft: 20,
    marginRight: 20,
    border: '1px solid #ff00008a',
    borderRadius: 20,
    textAlign: 'center',
    color: '#ff0000c4'
  },
  homeIconStyles: {
    marginLeft: window.innerWidth > 760 ? '15%' : '1%',
    marginRight: window.innerWidth > 760 ? '15%' : '1%',
    marginTop: 100,
    position: 'absolute'
  },
  formContent: {
    marginLeft: window.innerWidth > 760 ? '20%' : '7%',
    marginRight: window.innerWidth > 760 ? '20%' : '7%',
    marginTop: 15,
  },
  successMessageContainer: {
    width: '70%',
    marginLeft: '15%',
    // paddingTop: '15%',
    // paddingBottom: '15%',
    paddingLeft: 7,
    paddingTop: 7,
    // paddingRight: '5%',
    backgroundColor: '#07bb43a3',
    borderRadius: 21,
    // textAlign: 'center'
  },
  successMessage: {
    width: '70%',
    marginLeft: '15%',
    paddingTop: '9%',
    paddingBottom: 'calc(9% + 50px)',
    paddingLeft: '5%',
    paddingRight: '5%',
    // backgroundColor: '#07bb43a3',
    borderRadius: 21,
    textAlign: 'center'
  },
  successTitle: {
    color: 'white',
    fontSize: 26,
    fontWeight: 600
  },
  successDescription: {
    color: 'white'
  },
  successForm: {
    width: isMobile ? '100%' : 'inherit',
    marginLeft: isMobile ? 'inherit' : '17%',
    marginRight: '17%',
    marginTop: isMobile ? window.innerHeight * .2 : window.innerHeight * .05
  },
  logStyle: {
    width: '20%'
  },
  downloadAppStyle: {
    padding: 28
  }
}

class FormsHandler extends React.Component {

  constructor(props) {
    super(props)
    let { path, url } = this.props.match
    let formId = url?.split('forms/')[1] || '';
    let formConfig = FORMS_CONFIG.find((d) => d.formId === formId || (d.formId + '/') === formId) || null;
    console.log('formconfig', formConfig, formId, url)
    this.state = {
      formId,
      formConfig,
      formData: {},
      errorMessage: '',
      isFormSubmissionSuccess: false
    }
    if (!formId) {
      history.push('/')
    }
    if (!formConfig) {
      this.props.history.push('/page-not-found')
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.validateForm = this.validateForm.bind(this)
    this.onChange = this.onChange.bind(this)
    this.updateOptions = this.updateOptions.bind(this)
    this.updateFileSelected = this.updateFileSelected.bind(this)
    this.updateOptions()
  }

  updateOptions = async () => {
    if (!(this.state.formConfig?.formFields?.length)) {
      return
    }
    let formFields = this.state.formConfig.formFields
    for (let field of formFields) {
      if (field.optionsByAPI === 'getAllLanguages') {
        let options = await getAllLanguages();
        field.options = options
      }
      if (field.optionsByAPI === 'getAllAgeGroups') {
        field.options = await getAllAgeGroups()
      }

      if (field.optionsByAPI === 'getAllCountries') {
        field.options = await getAllCountries()
      }
    }
    let formConfig = this.state.formConfig
    formConfig.formFields = formFields
    this.setState({
      ...this.state,
      formConfig
    })
  }

  validateForm = () => {
    let errorMessage = null;
    for (let field of this.state.formConfig.formFields) {
      if(field.type === 'file') {
        continue
      }
      if (field.required || this.state.formData[field.key]) {
        let value = this.state.formData[field.key] ? '' + this.state.formData[field.key] : ''
        if (!value && !value.trim()) {
          errorMessage = `${field.label} should not be empty`
          return errorMessage
        }
        for (let validation of field.validations || []) {
          if (!validation.regex.test(value.trim().toLowerCase())) {
            errorMessage = validation.message
            break
          }
        }
        if (errorMessage) return errorMessage
      }
    }

    if (!errorMessage && this.state.formConfig.rules && this.state.formConfig.rules.length) {
      let rules = this.state.formConfig.rules
      for (let rule of rules) {
        if (rule.condition === 'equal' && (this.state.formData[rule.fields[0]] || this.state.formData[rule.fields[1]])) {
          let value1 = this.state.formData[rule.fields[0]] || ''
          let value2 = this.state.formData[rule.fields[1]] || ''
          if (value1.trim() !== value2.trim()) {
            errorMessage = rule.errorMessage
            return errorMessage
          }
        }
      }
    }

    let emailAndMobileFieldsExist = this.state.formConfig.formFields.filter((d) => ['email', 'mobileNumber'].includes(d.key));

    if (emailAndMobileFieldsExist.length && !(this.state.formData['email'] || this.state.formData['mobileNumber'])) {
      errorMessage = 'Please provide either of email address or mobile number'
    }

    return errorMessage;

  }

  onSubmit = async () => {
    let isValidationFailed = await this.validateForm();
    if (this.state.formConfig.consentDescription && !this.state.formData['termsAccepted']) {
      this.setState({
        ...this.state,
        errorMessage: 'Please check the consent to proceed'
      })
      return
    }
    if (isValidationFailed) {
      this.setState({
        ...this.state,
        errorMessage: isValidationFailed
      })
      return
    }

    let formData = new FormData();
    for (let key of Object.keys(this.state.formData)) {
      formData.append(key, this.state.formData[key])
    }
    formData.append('source', 'web')
    const FORMS = ['invitee-form', 'visitor-form', 'convert-form']
    if (FORMS.includes(this.state.formId)) {
      formData.append('formImage', null)
    }

    await axios.post(config.API_ENDPOINT + `/${this.state.formConfig.formIdAPI}`, formData)
      .then((res) => {
        if (res.status === 200 || res.status === 201) {
          this.setState({
            ...this.state,
            isFormSubmissionSuccess: true
          })
        }
      })

  }

  onChange(key, value) {
    let formData = this.state.formData
    formData[key] = value
    this.setState({
      ...this.state,
      formData,
      errorMessage: false
    })
  }

  updateFileSelected(keyName, fileContent) {
    let formData = this.state.formData
    formData[keyName] = fileContent
    this.setState({
      ...this.state,
      formData,
      errorMessage: false
    })
  }

  render() {
    if (this.state.formConfig && this.state.formConfig && !this.state.isFormSubmissionSuccess)
      return (
        <div style={{ overflowX: 'hidden', overflowY: 'auto' }}>
          <Banner />
          <div style={styles.homeIconStyles}>
            <img src={HomeIcon} width="50px" height="50px" style={{ cursor: 'pointer' }} onClick={(e) => this.props.history.push('/')} />
          </div>
          <h1 style={{ textAlign: 'center', marginTop: 100 }}>{this.state.formConfig.formName}</h1>
          <Row>
            <Col>
              <div className="form-button" style={styles.formContent}>
                <Row className="">
                  {
                    this.state.formConfig.formFields.map((field, fieldIndex) => {
                      return (
                        <Col key={fieldIndex} md={field.md} lg={field.lg} xl={field.xl} sm={field.sm} xs={field.xs}>
                          {
                            field.type === 'text' ? <HInput
                              label={field.label}
                              variant="outlined"
                              keyName={field.key}
                              value={this.state.formData[field.key]}
                              onChange={this.onChange}
                            /> : null
                          }
                          {
                            field.type === 'dropdown' ?
                              <Dropdown
                                label={field.label}
                                keyName={field.key}
                                onChange={this.onChange}
                                options={field.options}
                                optionLabel={field.optionLabel}
                                optionValue={field.optionValue}
                                value={this.state.formData[field.key]}
                              />
                              : null
                          }
                          {
                            field.type === 'textArea' ?
                              <HInput
                                label={field.label}
                                variant="outlined"
                                keyName={field.key}
                                value={this.state.formData[field.key]}
                                onChange={this.onChange}
                                multiline={true}
                                rows={5}
                              /> : null
                          }

                          {
                            field.type === 'file' ?
                              <FileUpload
                                label={field.label} 
                                keyName={field.key}
                                updateFileSelected={this.updateFileSelected}
                                accept={field.acceptFiles}
                              />: null
                          }

                        </Col>
                      )
                    })
                  }
                </Row>
                <Row className="d-flex justify-content-center" style={{ padding: 30, fontSize: 12 }}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        style={{ color: '#050527ba' }}
                        onChange={(e) => this.onChange('termsAccepted', e.target.checked)}
                      />}
                    label={this.state.formConfig.consentDescription}
                  />
                </Row>
                {this.state.errorMessage ?
                  <div className="error-message" style={styles.errorMessage}>
                    {this.state.errorMessage}
                  </div>
                  : null
                }
                <div className="d-flex justify-content-center">
                  <HButton disabled={this.state.errorMessage ? true : false} buttonValue="Submit" onSubmit={this.onSubmit} />
                </div>
                <div style={{ height: 50 }}>

                </div>
              </div>
            </Col>
          </Row>
        </div>
      )
    else if (this.state.formConfig && this.state.formConfig && this.state.isFormSubmissionSuccess) {
      return (
        <div className="form-success" style={styles.successForm}>
          <div className="d-flex justify-content-center">
            <img src={Logo} style={styles.logStyle} onClick={(e) => this.props.history.push('/')} />
          </div>
          <div style={styles.successMessageContainer}>
            <div>
              <img src={HomeIcon} width="50px" height="50px" style={{ cursor: 'pointer' }} onClick={(e) => this.props.history.push('/')} />
            </div>
            <div className="form-success-message" style={styles.successMessage}>
              <div className="form-success-title" style={styles.successTitle}>
                Thank You
          </div>
              <div className="form-success-description" style={styles.successDescription}>
                Record successfully submitted
              </div>
            </div>
          </div>
          {/* <div className="d-flex justify-content-center" style={styles.downloadAppStyle}>
            Now you can down load App
          </div> */}
          {/* <div className="d-flex justify-content-center">
            <img src={PlayStore} style={styles.logStyle} />
          </div> */}
        </div>
      )
    } else return null
  }
}

export default withRouter(FormsHandler)
