import React from 'react'
import { withRouter } from 'react-router-dom'

import Banner from '../Banner'

import convertFormSlip from '!!file-loader!../../assets/convert-form-slip.png'
import inviteeFormSlip from '!!file-loader!../../assets/invitee-form-slip.png'
import visitorFormSlip from '!!file-loader!../../assets/visitor-form-slip.png'
import testimonyFormSlip from '!!file-loader!../../assets/testimony-form-slip.png'

const isMobile = window.innerWidth < 760

const styles = {
  homePageContentItems: {
    marginLeft: window.innerWidth > 760 ? '18%' : '7%',
    marginRight: window.innerWidth > 760 ? '18%' : '7%'
  },
  homePageContentItem: {
    cursor: 'pointer'
  },
  homePageContentHeader: {
    color: '#000000',
    fontStyle: 'normal',
    fontSize: 30,
    fontWeight: 600,
    marginTop: 110,
    textAlign: 'center'
  },
  formSlipImage: {
    height: 110,
    width: 110,
    borderRadius: 20
  },
  contentItemTitle: {
    fontWeight: 600,
    color: '#000000',
    fontSize: 40
  },
  itemContentTitle: {
    fontSize: isMobile ? 18 : 20,
    fontWeight: 600
  },
  itemContentDescription: {
    fontSize: isMobile ? 10 : 12
  }
}

class HomePage extends React.Component {
  constructor(props) {
    super(props)
    this.formSelected = this.formSelected.bind(this)
  }

  formSelected = (formName) => {
    this.props.history.push(`/forms/${formName}`)
  }

  render() {
    return (
      <div className="home-page">
        <Banner />
        <div className="home-page-content">
          <div className="home-page-content-header" style={styles.homePageContentHeader}>
            What Do You Want To Do?
          </div>
          <div className="home-page-content-items" style={styles.homePageContentItems}>
            <div className="row home-page-content-item">
              {/* convert form */}
              <div onClick={(e) => { this.formSelected('convert-form') }} style={styles.homePageContentItem} className="col-md-6 col-lg-6 col-xl-6 col-12">
                <div className="row">
                  <div className="col-md-4 col-lg-4 col-xl-4 col-4 home-page-content-item-image" style={{ paddingTop: 40 }}>
                    <img src={convertFormSlip} style={styles.formSlipImage} />
                  </div>
                  <div className="col-sm-offset-1 col-xs-offset-1 col-md-8 col-lg-8 col-xl-8 col-8 home-page-content-item-content" style={{ paddingTop: 40 }}>
                    <div className="home-page-content-item-content-title" style={styles.itemContentTitle}>
                      Fill Convert Slip Form
                    </div>
                    <div className="home-page-content-item-content-description" style={styles.itemContentDescription}>
                      Use this form to submit your details for followup by the Church if you have just given your life to Jesus Christ as your Lord and Saviour during this programme.
                    </div>
                  </div>
                </div>
              </div>

              {/* invitee form */}
              <div onClick={(e) => { this.formSelected('invitee-form') }} style={styles.homePageContentItem} className="col-md-6 col-lg-6 col-xl-6 col-sm-12">
                <div className="row">
                  <div className="col-md-4 col-lg-4 col-xl-4 col-4 home-page-content-item-image" style={{ paddingTop: 40 }}>
                    <img src={inviteeFormSlip} style={styles.formSlipImage} />
                  </div>
                  <div className="col-sm-offset-1 col-xs-offset-1 col-md-8 col-lg-8 col-xl-8 col-8 home-page-content-item-content" style={{ paddingTop: 40 }}>
                    <div className="home-page-content-item-content-title" style={styles.itemContentTitle}>
                      Submit Invitee
                    </div>
                    <div className="home-page-content-item-content-description" style={styles.itemContentDescription}>
                      If you are inviting someone or you have been invited by a member of the Church, use this form to submit details of your invitee for further follow up.
                    </div>
                  </div>
                </div>
              </div>

              {/* visitor form */}
              <div onClick={(e) => { this.formSelected('visitor-form') }} style={styles.homePageContentItem} className="col-md-6 col-lg-6 col-xl-6 col-sm-12">
                <div className="row">
                  <div className="col-md-4 col-lg-4 col-xl-4 col-4 home-page-content-item-image" style={{ paddingTop: 40 }}>
                    <img src={visitorFormSlip} style={styles.formSlipImage} />
                  </div>
                  <div className="col-sm-offset-1 col-xs-offset-1 col-md-8 col-lg-8 col-xl-8 col-7 home-page-content-item-content" style={{ paddingTop: 40 }}>
                    <div className="home-page-content-item-content-title" style={styles.itemContentTitle}>
                      Fill Visitor Slip Form
                    </div>
                    <div className="home-page-content-item-content-description" style={styles.itemContentDescription}>
                      Use this form to indicate if this is your first time of attending a Deeper Life programme so that our nearest Church can reach out to you for more fellowship with the brethren.
                    </div>
                  </div>
                </div>
              </div>

              {/* testimony form */}
              <div onClick={(e) => { this.formSelected('testimony-form') }} style={styles.homePageContentItem} className="col-md-6 col-lg-6 col-xl-6 col-sm-12">
                <div className="row">
                  <div className="col-md-4 col-lg-4 col-xl-4 col-4 home-page-content-item-image" style={{ paddingTop: 40 }}>
                    <img src={testimonyFormSlip} style={styles.formSlipImage} />
                  </div>
                  <div className="col-sm-offset-1 col-xs-offset-1 col-md-8 col-lg-8 col-xl-8 col-7 home-page-content-item-content" style={{ paddingTop: 40 }}>
                    <div className="home-page-content-item-content-title" style={styles.itemContentTitle}>
                      View Or Submit Testimony
                    </div>
                    <div className="home-page-content-item-content-description" style={styles.itemContentDescription}>
                      Use this page to share with us your testimony or read about miracles others have by received by the touch of the Almighty God during this programme.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

}

export default withRouter(HomePage)
