import React from 'react'

const styles = {
  formFooter: {
    width: '98%',
    backgroundColor: '#050527ba',
    minHeight: window.innerHeight * .3,
    borderRadius: 20,
    marginLeft: '1%',
    marginTop: window.innerHeight * .05,
    marginBottom: window.innerHeight * .05,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
    paddingBottom: 20
  },
  header1Styles: {
    color: 'white',
  },
  header2Styles: {
    color: '#ffffff69'
  },
  contentStyles: {
    color: 'white',
    fontSize: 11,

  }
}

export default class FormFooter extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return(
      <div style={styles.formFooter}>
        <h4 style={styles.header1Styles}>{this.props.name}</h4>
        <h5 style={styles.header2Styles}>{this.props.tagLine}</h5>
        <div style={styles.contentStyles}>
          {this.props.content}
        </div>
      </div>
    )
  }

}
