
import React from 'react'

const styles = {
  errorMessage: {
    paddingTop: '25%',
    paddingBottom: '25%',
    paddingLeft: '10%',
    paddingRight: '10%',
    backgroundColor: '#ff00009e',
    borderRadius: 21,
    textAlign: 'center'
  },
  errorTitle: {
    color: 'white',
    fontSize: 26,
    fontWeight: 600
  },
  errorDescription: {
    color: 'white'
  },
  errorForm: {
    marginLeft: '17%',
    marginRight: '17%',
    marginTop: window.outerWidth < 760 ? window.innerHeight * .33 : window.innerHeight * .2
  }
}

export default class NotFoundPage extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div className="form-error" style={styles.errorForm}>
        <div className="form-error-message" style={styles.errorMessage}>
          <div className="form-error-title" style={styles.errorTitle}>
            Page Not Found
          </div>
          <div className="form-error-description" style={styles.errorDescription}>
            Please go back to home page
          </div>
        </div>
      </div>
    )
  }

}
