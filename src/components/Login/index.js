import React from 'react'
import HButton from '../../form-components/HButton'
import HInput from '../../form-components/HInput'

const styles = {
  loginPage: {

  },
  loginForm: {

  },
  loginFormHeader: {
    padding: 10,
    fontSize: 40,
    color: '#484861',
    fontWeight: 1000
  },
  deeperLogo: {
    height: 100,
    textAlign: 'center',
    padding: 40
  },
  loginInputs: {
    paddingTop: 10,
    paddingBottom: 10
  }
}

const isMobile = window.innerWidth < 760 ? true : false

export default class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: null,
      password: null
    }
    this.renderLoginForm = this.renderLoginForm.bind(this)
  }

  onChange = (keyName, value) => {
    this.setState({
      ...this.state,
      [keyName]: value
    })
  }

  renderLoginForm = () => {
    return (
      <div className="login-form" style={isMobile ? { padding: '10%' } : null}>
        <div className="deeper-logo" style={styles.deeperLogo}>
          Logo goes here
        </div>
        <div className="login-form-header" style={styles.loginFormHeader}>
          Login
        </div>
        <div className="login-form-username" style={styles.loginInputs}>
          <HInput label="Username" type="text" keyName="username" onChange={this.onChange} />
        </div>
        <div className="login-form-password" style={styles.loginInputs}>
          <HInput label="Password" type="password" keyName="password" onChange={this.onChange} />
        </div>
      </div>
    )
  }

  render() {
    return (
      <div>
        {
          isMobile ?
            this.renderLoginForm()
            :
            <div className="login-page">
              {this.renderLoginForm()}
            </div>
        }
        <div className="login-form-submission" style={isMobile ? { paddingLeft: '10%' } : null}>
          <HButton buttonValue="Login" />
        </div>
      </div>
    )
  }
}
