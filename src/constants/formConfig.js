import COUNTRY_CODES from './contryCodes'

export default [
  {
    formName: 'Convert Form',
    formId: 'convert-form',
    formIdAPI: 'createConvert',
    tagLine: 'Lorem ipsum dolor sit amet',
    bannerImage: '',
    description: '',
    successDescription: 'You Have Added The New Convert Successfully',
    consentDescription: 'Please, note that it is voluntary and we take this as your consent to keep your data solely for the purpose of contacting you to help you grow in the Christian faith',
    contentData: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt risus vel ligula rutrum, at tincidunt neque laoreet. Integer vitae mollis magna. Nulla facilisi. Pellentesque tempus suscipit felis, id tincidunt risus aliquam eget. Etiam ultrices congue arcu. Nullam tincidunt pharetra augue. Ut pretium, quam et scelerisque lacinia, nunc enim bibendum nibh, eget blandit lectus dolor vel lorem. Mauris tempor sodales ex, sed egestas eros viverra a',
    formFields: [
      {
        label: 'First Name',
        key: 'firstName',
        type: 'text',
        required: true,
        order: 1,
        // validations: [
        //   {
        //     regex: /^[a-zA-Z]{1,30}$/,
        //     message: 'First name should be at least 1 letter and max 30'
        //   }
        // ],
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Last Name',
        key: 'lastName',
        type: 'text',
        required: true,
        order: 2,
        // validations: [
        //   {
        //     regex: /^[a-zA-Z]{1,30}$/,
        //     message: 'Last name should be at least 1 letter and max 30'
        //   }
        // ],
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Alias (Popular Name)',
        key: 'alias',
        type: 'text',
        required: false,
        order: 2,
        // validations: [
        //   {
        //     regex: /^[a-zA-Z]{1,30}$/,
        //     message: 'Last name should be at least 1 letter and max 30'
        //   }
        // ],
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Gender',
        key: 'gender',
        type: 'dropdown',
        required: false,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: [
          {
            label: 'Male',
            value: 'male'
          }, {
            label: 'Female',
            value: 'female'
          }
        ],
        optionLabel: 'label',
        optionValue: 'value',
        doSendToAPI: true
      },
      {
        label: 'Age Category',
        key: 'ageGroupId',
        type: 'dropdown',
        required: false,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: [],
        optionLabel: 'label',
        optionValue: 'value',
        doSendToAPI: true,
        optionsByAPI: 'getAllAgeGroups'
      },
      {
        label: 'Preferred Language',
        key: 'preferredLanguage',
        type: 'text',
        required: false,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: [],
        optionLabel: 'name',
        optionValue: 'languageId',
        doSendToAPI: true,
        optionsByAPI: 'getAllLanguages'
      },
      {
        label: 'Mobile Number',
        key: 'mobile',
        type: 'text',
        required: false,
        // validations: [
        //   {
        //     regex: /^[0-9]{10-14}$/,
        //     message: 'Invalid mobile number. Number should contain only numbers from 10 to 14 digits.'
        //   }
        // ],
        order: 4,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Confirm Mobile Number',
        key: 'confirmMobile',
        type: 'text',
        required: false,
        // validations: [
        //   {
        //     regex: /^[0-9]{10-14}$/,
        //     message: 'Invalid mobile number. Number should contain only numbers from 10 to 14 digits.'
        //   }
        // ],
        order: 4,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: false
      },
      {
        label: 'Email Address',
        key: 'email',
        type: 'text',
        required: false,
        // validations: [
        //   {
        //     regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        //     message: 'Invalid email address'
        //   }
        // ],
        order: 5,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Confirm Email Address',
        key: 'confirmEmail',
        type: 'text',
        required: false,
        // validations: [
        //   {
        //     regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        //     message: 'Invalid email address'
        //   }
        // ],
        order: 5,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: false
      },
      {
        label: 'Country',
        key: 'countryCode',
        type: 'dropdown',
        required: true,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: COUNTRY_CODES,
        optionLabel: 'name',
        optionValue: 'code',
        doSendToAPI: true,
        // optionsByAPI: 'getAllCountries'
      },
      {
        label: 'State/Province',
        key: 'state',
        type: 'text',
        required: true,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Street Address',
        key: 'address',
        type: 'text',
        required: false,
        order: 6,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Code/Zip Code',
        key: 'zipCode',
        type: 'text',
        required: false,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true,
        options: COUNTRY_CODES,
        optionLabel: 'name',
        optionValue: 'dial',
        // validations: [
        //   {
        //     regex: /^[0-9]{6}$/,
        //     message: 'Invalid email address'
        //   }
        // ]
      },
    ],
    rules: [
      {
        condition: 'equal',
        fields: ['mobile', 'confirmMobile'],
        errorMessage: 'Mobile Number and Confirm Mobile Number are mismatched'
      },
      {
        condition: 'equal',
        fields: ['email', 'confirmEmail'],
        errorMessage: 'Email and Confirm Email are mismatched'
      }
    ]
  },
  // {
  //   formName: 'Invitee Form',
  //   formId: 'invitee-form',
  //   formIdAPI: 'createInvitee',
  //   tagLine: 'Lorem ipsum dolor sit amet',
  //   bannerImage: '',
  //   description: '',
  //   successDescription: 'You Have Added The New Convert Successfully',
  //   consentDescription: 'Please, note that it is voluntary and we take this as your consent to keep your data solely for the purpose of contacting you to help you grow in the Christian faith',
  //   contentData: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt risus vel ligula rutrum, at tincidunt neque laoreet. Integer vitae mollis magna. Nulla facilisi. Pellentesque tempus suscipit felis, id tincidunt risus aliquam eget. Etiam ultrices congue arcu. Nullam tincidunt pharetra augue. Ut pretium, quam et scelerisque lacinia, nunc enim bibendum nibh, eget blandit lectus dolor vel lorem. Mauris tempor sodales ex, sed egestas eros viverra a',
  //   formFields: [
  //     {
  //       label: 'First Name',
  //       key: 'firstName',
  //       type: 'text',
  //       required: true,
  //       order: 1,
  //       validations: [
  //         {
  //           regex: /^[a-zA-Z]{1,30}$/,
  //           message: 'First name should be at least 1 letter and max 30'
  //         }
  //       ],
  //       xl: 6,
  //       lg: 6,
  //       md: 6,
  //       xs: 12,
  //       sm: 12,
  //       doSendToAPI: true
  //     },
  //     {
  //       label: 'Last Name',
  //       key: 'lastName',
  //       type: 'text',
  //       required: true,
  //       order: 2,
  //       validations: [
  //         {
  //           regex: /^[a-zA-Z]{1,30}$/,
  //           message: 'Last name should be at least 1 letter and max 30'
  //         }
  //       ],
  //       xl: 6,
  //       lg: 6,
  //       md: 6,
  //       xs: 12,
  //       sm: 12,
  //       doSendToAPI: true
  //     },
  //     {
  //       label: 'Email Address',
  //       key: 'email',
  //       type: 'text',
  //       required: false,
  //       validations: [
  //         {
  //           regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  //           message: 'Invalid email address'
  //         }
  //       ],
  //       order: 5,
  //       xl: 6,
  //       lg: 6,
  //       md: 6,
  //       xs: 12,
  //       sm: 12,
  //       doSendToAPI: true
  //     },
  //     {
  //       label: 'Confirm Email Address',
  //       key: 'confirmEmail',
  //       type: 'text',
  //       required: false,
  //       validations: [
  //         {
  //           regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  //           message: 'Invalid email address'
  //         }
  //       ],
  //       order: 5,
  //       xl: 6,
  //       lg: 6,
  //       md: 6,
  //       xs: 12,
  //       sm: 12,
  //       doSendToAPI: false
  //     },
  //     {
  //       label: 'Mobile Number',
  //       key: 'mobile',
  //       type: 'text',
  //       required: false,
  //       validations: [
  //         {
  //           regex: /^[0-9]{1,10}$/,
  //           message: 'Invalid mobile number'
  //         }
  //       ],
  //       order: 4,
  //       xl: 6,
  //       lg: 6,
  //       md: 6,
  //       xs: 12,
  //       sm: 12,
  //       doSendToAPI: true
  //     },
  //     {
  //       label: 'Confirm Mobile Number',
  //       key: 'confirmMobile',
  //       type: 'text',
  //       required: false,
  //       validations: [
  //         {
  //           regex: /^[0-9]{1,10}$/,
  //           message: 'Invalid mobile number'
  //         }
  //       ],
  //       order: 4,
  //       xl: 6,
  //       lg: 6,
  //       md: 6,
  //       xs: 12,
  //       sm: 12,
  //       doSendToAPI: false
  //     },
  //     {
  //       label: 'Gender',
  //       key: 'gender',
  //       type: 'dropdown',
  //       required: false,
  //       order: 3,
  //       xl: 6,
  //       lg: 6,
  //       md: 6,
  //       xs: 12,
  //       sm: 12,
  //       options: [
  //         {
  //           label: 'Male',
  //           value: 'male'
  //         }, {
  //           label: 'Female',
  //           value: 'female'
  //         }
  //       ],
  //       optionLabel: 'label',
  //       optionValue: 'value',
  //       doSendToAPI: true
  //     },
  //     {
  //       label: 'Age Category',
  //       key: 'ageGroupId',
  //       type: 'dropdown',
  //       required: false,
  //       order: 3,
  //       xl: 6,
  //       lg: 6,
  //       md: 6,
  //       xs: 12,
  //       sm: 12,
  //       options: [],
  //       optionLabel: 'label',
  //       optionValue: 'value',
  //       doSendToAPI: true,
  //       optionsByAPI: 'getAllAgeGroups'
  //     },
  //     {
  //       label: 'Preferred Language',
  //       key: 'preferredLanguage',
  //       type: 'text',
  //       required: false,
  //       order: 3,
  //       xl: 6,
  //       lg: 6,
  //       md: 6,
  //       xs: 12,
  //       sm: 12,
  //       options: [],
  //       optionLabel: 'name',
  //       optionValue: 'languageId',
  //       doSendToAPI: true,
  //       optionsByAPI: 'getAllLanguages'
  //     },
  //     {
  //       label: 'Country',
  //       key: 'countryCode',
  //       type: 'dropdown',
  //       required: true,
  //       order: 3,
  //       xl: 6,
  //       lg: 6,
  //       md: 6,
  //       xs: 12,
  //       sm: 12,
  //       options: [],
  //       optionLabel: 'name',
  //       optionValue: 'code',
  //       doSendToAPI: true,
  //       optionsByAPI: 'getAllCountries'
  //     },
  //     {
  //       label: 'State',
  //       key: 'countryCode',
  //       type: 'text',
  //       required: true,
  //       order: 3,
  //       xl: 6,
  //       lg: 6,
  //       md: 6,
  //       xs: 12,
  //       sm: 12,
  //       doSendToAPI: true
  //     }
  //   ],
  //   rules: [
  //     {
  //       condition: 'equal',
  //       fields: ['mobile', 'confirmMobile'],
  //       errorMessage: 'Mobile Number and Confirm Mobile Number are mismatched'
  //     },
  //     {
  //       condition: 'equal',
  //       fields: ['email', 'confirmEmail'],
  //       errorMessage: 'Email and Confirm Email are mismatched'
  //     }
  //   ]
  // },
  {
    formName: 'Invitee Form',
    formId: 'invitee-form',
    formIdAPI: 'createInvitee',
    tagLine: 'Lorem ipsum dolor sit amet',
    bannerImage: '',
    description: '',
    successDescription: 'You Have Added The New Invitee Successfully',
    consentDescription: 'Please, note that it is voluntary and we take this as your consent to keep your data solely for the purpose of contacting you to help you grow in the Christian faith',
    contentData: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt risus vel ligula rutrum, at tincidunt neque laoreet. Integer vitae mollis magna. Nulla facilisi. Pellentesque tempus suscipit felis, id tincidunt risus aliquam eget. Etiam ultrices congue arcu. Nullam tincidunt pharetra augue. Ut pretium, quam et scelerisque lacinia, nunc enim bibendum nibh, eget blandit lectus dolor vel lorem. Mauris tempor sodales ex, sed egestas eros viverra a',
    formFields: [
      {
        label: 'First Name',
        key: 'firstName',
        type: 'text',
        required: true,
        order: 1,
        // validations: [
        //   {
        //     regex: /^[a-zA-Z]{1,30}$/,
        //     message: 'First name should be at least 1 letter and max 30'
        //   }
        // ],
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Last Name',
        key: 'lastName',
        type: 'text',
        required: true,
        order: 2,
        // validations: [
        //   {
        //     regex: /^[a-zA-Z]{1,30}$/,
        //     message: 'Last name should be at least 1 letter and max 30'
        //   }
        // ],
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Alias (Popular Name)',
        key: 'alias',
        type: 'text',
        required: false,
        order: 2,
        // validations: [
        //   {
        //     regex: /^[a-zA-Z]{1,30}$/,
        //     message: 'Last name should be at least 1 letter and max 30'
        //   }
        // ],
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Gender',
        key: 'gender',
        type: 'dropdown',
        required: false,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: [
          {
            label: 'Male',
            value: 'male'
          }, {
            label: 'Female',
            value: 'female'
          }
        ],
        optionLabel: 'label',
        optionValue: 'value',
        doSendToAPI: true
      },
      {
        label: 'Age Category',
        key: 'ageGroupId',
        type: 'dropdown',
        required: false,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: [],
        optionLabel: 'label',
        optionValue: 'value',
        doSendToAPI: true,
        optionsByAPI: 'getAllAgeGroups'
      },
      {
        label: 'Preferred Language',
        key: 'preferredLanguage',
        type: 'text',
        required: false,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: [],
        optionLabel: 'name',
        optionValue: 'languageId',
        doSendToAPI: true,
        optionsByAPI: 'getAllLanguages'
      },
      {
        label: 'Mobile Number',
        key: 'mobile',
        type: 'text',
        required: false,
        // validations: [
        //   {
        //     regex: /^[0-9]{10-14}$/,
        //     message: 'Invalid mobile number. Number should contain only numbers from 10 to 14 digits.'
        //   }
        // ],
        order: 4,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Confirm Mobile Number',
        key: 'confirmMobile',
        type: 'text',
        required: false,
        // validations: [
        //   {
        //     regex: /^[0-9]{10-14}$/,
        //     message: 'Invalid mobile number. Number should contain only numbers from 10 to 14 digits.'
        //   }
        // ],
        order: 4,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: false
      },
      {
        label: 'Email Address',
        key: 'email',
        type: 'text',
        required: false,
        // validations: [
        //   {
        //     regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        //     message: 'Invalid email address'
        //   }
        // ],
        order: 5,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Confirm Email Address',
        key: 'confirmEmail',
        type: 'text',
        required: false,
        // validations: [
        //   {
        //     regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        //     message: 'Invalid email address'
        //   }
        // ],
        order: 5,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: false
      },
      {
        label: 'Country',
        key: 'countryCode',
        type: 'dropdown',
        required: true,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: COUNTRY_CODES,
        optionLabel: 'name',
        optionValue: 'code',
        doSendToAPI: true,
        // optionsByAPI: 'getAllCountries'
      },
      {
        label: 'State/Province',
        key: 'state',
        type: 'text',
        required: true,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Street Address',
        key: 'address',
        type: 'text',
        required: false,
        order: 6,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Code/Zip Code',
        key: 'zipCode',
        type: 'text',
        required: false,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true,
        options: COUNTRY_CODES,
        optionLabel: 'name',
        optionValue: 'dial',
        // validations: [
        //   {
        //     regex: /^[0-9]{6}$/,
        //     message: 'Invalid email address'
        //   }
        // ]
      },
    ],
    rules: [
      {
        condition: 'equal',
        fields: ['mobile', 'confirmMobile'],
        errorMessage: 'Mobile Number and Confirm Mobile Number are mismatched'
      },
      {
        condition: 'equal',
        fields: ['email', 'confirmEmail'],
        errorMessage: 'Email and Confirm Email are mismatched'
      }
    ]
  },
  {
    formName: 'Vistitors Form',
    formId: 'visitor-form',
    formIdAPI: 'createVistor',
    tagLine: 'Lorem ipsum dolor sit amet',
    bannerImage: '',
    description: '',
    successDescription: 'You Have Added The New Visitor Successfully',
    consentDescription: 'Please, note that it is voluntary and we take this as your consent to keep your data solely for the purpose of contacting you to help you grow in the Christian faith',
    contentData: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt risus vel ligula rutrum, at tincidunt neque laoreet. Integer vitae mollis magna. Nulla facilisi. Pellentesque tempus suscipit felis, id tincidunt risus aliquam eget. Etiam ultrices congue arcu. Nullam tincidunt pharetra augue. Ut pretium, quam et scelerisque lacinia, nunc enim bibendum nibh, eget blandit lectus dolor vel lorem. Mauris tempor sodales ex, sed egestas eros viverra a',
    formFields: [
      {
        label: 'First Name',
        key: 'firstName',
        type: 'text',
        required: true,
        order: 1,
        // validations: [
        //   {
        //     regex: /^[a-zA-Z]{1,30}$/,
        //     message: 'First name should be at least 1 letter and max 30'
        //   }
        // ],
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Last Name',
        key: 'lastName',
        type: 'text',
        required: true,
        order: 2,
        // validations: [
        //   {
        //     regex: /^[a-zA-Z]{1,30}$/,
        //     message: 'Last name should be at least 1 letter and max 30'
        //   }
        // ],
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Alias (Popular Name)',
        key: 'alias',
        type: 'text',
        required: false,
        order: 2,
        // validations: [
        //   {
        //     regex: /^[a-zA-Z]{1,30}$/,
        //     message: 'Last name should be at least 1 letter and max 30'
        //   }
        // ],
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Gender',
        key: 'gender',
        type: 'dropdown',
        required: false,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: [
          {
            label: 'Male',
            value: 'male'
          }, {
            label: 'Female',
            value: 'female'
          }
        ],
        optionLabel: 'label',
        optionValue: 'value',
        doSendToAPI: true
      },
      {
        label: 'Age Category',
        key: 'ageGroupId',
        type: 'dropdown',
        required: false,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: [],
        optionLabel: 'label',
        optionValue: 'value',
        doSendToAPI: true,
        optionsByAPI: 'getAllAgeGroups'
      },
      {
        label: 'Preferred Language',
        key: 'preferredLanguage',
        type: 'text',
        required: false,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: [],
        optionLabel: 'name',
        optionValue: 'languageId',
        doSendToAPI: true,
        optionsByAPI: 'getAllLanguages'
      },
      {
        label: 'Mobile Number',
        key: 'mobile',
        type: 'text',
        required: false,
        // validations: [
        //   {
        //     regex: /^[0-9]{10-14}$/,
        //     message: 'Invalid mobile number. Number should contain only numbers from 10 to 14 digits.'
        //   }
        // ],
        order: 4,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Confirm Mobile Number',
        key: 'confirmMobile',
        type: 'text',
        required: false,
        // validations: [
        //   {
        //     regex: /^[0-9]{10-14}$/,
        //     message: 'Invalid mobile number. Number should contain only numbers from 10 to 14 digits.'
        //   }
        // ],
        order: 4,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: false
      },
      {
        label: 'Email Address',
        key: 'email',
        type: 'text',
        required: false,
        // validations: [
        //   {
        //     regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        //     message: 'Invalid email address'
        //   }
        // ],
        order: 5,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Confirm Email Address',
        key: 'confirmEmail',
        type: 'text',
        required: false,
        // validations: [
        //   {
        //     regex: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        //     message: 'Invalid email address'
        //   }
        // ],
        order: 5,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: false
      },
      {
        label: 'Country',
        key: 'countryCode',
        type: 'dropdown',
        required: true,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: COUNTRY_CODES,
        optionLabel: 'name',
        optionValue: 'code',
        doSendToAPI: true,
        // optionsByAPI: 'getAllCountries'
      },
      {
        label: 'State/Province',
        key: 'state',
        type: 'text',
        required: true,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Street Address',
        key: 'address',
        type: 'text',
        required: false,
        order: 6,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Code/Zip Code',
        key: 'zipCode',
        type: 'text',
        required: false,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true,
        options: COUNTRY_CODES,
        optionLabel: 'name',
        optionValue: 'dial',
        // validations: [
        //   {
        //     regex: /^[0-9]{6}$/,
        //     message: 'Invalid email address'
        //   }
        // ]
      },
    ],
    rules: [
      {
        condition: 'equal',
        fields: ['mobile', 'confirmMobile'],
        errorMessage: 'Mobile Number and Confirm Mobile Number are mismatched'
      },
      {
        condition: 'equal',
        fields: ['email', 'confirmEmail'],
        errorMessage: 'Email and Confirm Email are mismatched'
      }
    ]
  },
  {
    formName: 'New Testimony',
    formId: 'testimony-form',
    formIdAPI: 'createTestimony',
    tagLine: 'Lorem ipsum dolor sit amet',
    bannerImage: '',
    description: '',
    successDescription: 'You Have Added The New Testimony Successfully',
    consentDescription: 'I certify that the information provided above and any attached documents is true and correct.',
    contentData: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tincidunt risus vel ligula rutrum, at tincidunt neque laoreet. Integer vitae mollis magna. Nulla facilisi. Pellentesque tempus suscipit felis, id tincidunt risus aliquam eget. Etiam ultrices congue arcu. Nullam tincidunt pharetra augue. Ut pretium, quam et scelerisque lacinia, nunc enim bibendum nibh, eget blandit lectus dolor vel lorem. Mauris tempor sodales ex, sed egestas eros viverra a',
    formFields:[
      {
        label: 'First Name',
        key: 'firstName',
        type: 'text',
        required: true,
        order: 1,
        // validations: [
        //   {
        //     regex: /^[a-zA-Z]{1,30}$/,
        //     message: 'First name should be at least 1 letter and max 30'
        //   }
        // ],
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Last Name',
        key: 'lastName',
        type: 'text',
        required: true,
        order: 2,
        // validations: [
        //   {
        //     regex: /^[a-zA-Z]{1,30}$/,
        //     message: 'Last name should be at least 1 letter and max 30'
        //   }
        // ],
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Enter Your Testimony',
        key: 'body',
        type: 'textArea',
        required: false,
        order: 1,
        // validations: [
        //   {
        //     regex: /^[a-zA-Z]{1,500}$/,
        //     message: 'First name should be at least 1 letter and max 30'
        //   }
        // ],
        xl: 12,
        lg: 12,
        md: 12,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Add Audio',
        key: 'audio',
        type: 'file',
        required: false,
        acceptFiles: "audio/*",
        xl: 4,
        lg: 4,
        md: 4,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Add Video',
        key: 'video',
        type: 'file',
        required: false,
        acceptFiles: "video/*",
        xl: 4,
        lg: 4,
        md: 4,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Add Photo',
        key: 'photo',
        type: 'file',
        required: false,
        acceptFiles: "image/*",
        xl: 4,
        lg: 4,
        md: 4,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Country',
        key: 'countryCode',
        type: 'dropdown',
        required: true,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: COUNTRY_CODES,
        optionLabel: 'name',
        optionValue: 'code',
        doSendToAPI: true,
        // optionsByAPI: 'getAllCountries'
      },
      {
        label: 'State/Province',
        key: 'state',
        type: 'text',
        required: true,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        doSendToAPI: true
      },
      {
        label: 'Gender',
        key: 'gender',
        type: 'dropdown',
        required: false,
        order: 3,
        xl: 6,
        lg: 6,
        md: 6,
        xs: 12,
        sm: 12,
        options: [
          {
            label: 'Male',
            value: 'male'
          }, {
            label: 'Female',
            value: 'female'
          }
        ],
        optionLabel: 'label',
        optionValue: 'value',
        doSendToAPI: true
      }
    ],
  }
]