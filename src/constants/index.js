import COUNTRY_CODES from './contryCodes';

import FORMS_CONFIG from './formConfig'

export {
  FORMS_CONFIG,
  COUNTRY_CODES
}
