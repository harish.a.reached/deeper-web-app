
export default [
  {
      "name": "Taiwan",
      "dial": "886",
      "code": "RC"
  },
  {
      "name": "Afghanistan",
      "dial": "93",
      "code": "AFG"
  },
  {
      "name": "Albania",
      "dial": "355",
      "code": "AL"
  },
  {
      "name": "Algeria",
      "dial": "213",
      "code": "DZ"
  },
  {
      "name": "American Samoa",
      "dial": "1-684",
      "code": "USA"
  },
  {
      "name": "Andorra",
      "dial": "376",
      "code": "AND"
  },
  {
      "name": "Angola",
      "dial": "244",
      "code": "AO"
  },
  {
      "name": "Anguilla",
      "dial": "1-264",
      "code": null
  },
  {
      "name": "Antarctica",
      "dial": "672",
      "code": null
  },
  {
      "name": "Antigua & Barbuda",
      "dial": "1-268",
      "code": null
  },
  {
      "name": "Argentina",
      "dial": "54",
      "code": "RA"
  },
  {
      "name": "Armenia",
      "dial": "374",
      "code": "AM"
  },
  {
      "name": "Aruba",
      "dial": "297",
      "code": "AW"
  },
  {
      "name": "Australia",
      "dial": "61",
      "code": "AUS"
  },
  {
      "name": "Austria",
      "dial": "43",
      "code": "A"
  },
  {
      "name": "Azerbaijan",
      "dial": "994",
      "code": "AZ"
  },
  {
      "name": "Bahamas",
      "dial": "1-242",
      "code": "BS"
  },
  {
      "name": "Bahrain",
      "dial": "973",
      "code": "BRN"
  },
  {
      "name": "Bangladesh",
      "dial": "880",
      "code": "BD"
  },
  {
      "name": "Barbados",
      "dial": "1-246",
      "code": "BDS"
  },
  {
      "name": "Belarus",
      "dial": "375",
      "code": "BY"
  },
  {
      "name": "Belgium",
      "dial": "32",
      "code": "B"
  },
  {
      "name": "Belize",
      "dial": "501",
      "code": "BH"
  },
  {
      "name": "Benin",
      "dial": "229",
      "code": "DY"
  },
  {
      "name": "Bermuda",
      "dial": "1-441",
      "code": "BM"
  },
  {
      "name": "Bhutan",
      "dial": "975",
      "code": "BT"
  },
  {
      "name": "Bolivia",
      "dial": "591",
      "code": "BOL"
  },
  {
      "name": "Caribbean Netherlands",
      "dial": "599",
      "code": "NA"
  },
  {
      "name": "Bosnia",
      "dial": "387",
      "code": "BIH"
  },
  {
      "name": "Botswana",
      "dial": "267",
      "code": "BW"
  },
  {
      "name": "Bouvet Island",
      "dial": "47",
      "code": "BV"
  },
  {
      "name": "Brazil",
      "dial": "55",
      "code": "BR"
  },
  {
      "name": "British Indian Ocean Territory",
      "dial": "246",
      "code": null
  },
  {
      "name": "British Virgin Islands",
      "dial": "1-284",
      "code": "BVI"
  },
  {
      "name": "Brunei",
      "dial": "673",
      "code": "BRU"
  },
  {
      "name": "Bulgaria",
      "dial": "359",
      "code": "BG"
  },
  {
      "name": "Burkina Faso",
      "dial": "226",
      "code": "BF"
  },
  {
      "name": "Burundi",
      "dial": "257",
      "code": "RU"
  },
  {
      "name": "Cape Verde",
      "dial": "238",
      "code": "CV"
  },
  {
      "name": "Cambodia",
      "dial": "855",
      "code": "K"
  },
  {
      "name": "Cameroon",
      "dial": "237",
      "code": "CAM"
  },
  {
      "name": "Canada",
      "dial": "1",
      "code": "CDN"
  },
  {
      "name": "Cayman Islands",
      "dial": "1-345",
      "code": "KY"
  },
  {
      "name": "Central African Republic",
      "dial": "236",
      "code": "RCA"
  },
  {
      "name": "Chad",
      "dial": "235",
      "code": "TCH"
  },
  {
      "name": "Chile",
      "dial": "56",
      "code": "RCH"
  },
  {
      "name": "China",
      "dial": "86",
      "code": "CN"
  },
  {
      "name": "Hong Kong",
      "dial": "852",
      "code": "HK"
  },
  {
      "name": "Macau",
      "dial": "853",
      "code": "MO"
  },
  {
      "name": "Christmas Island",
      "dial": "61",
      "code": "AUS"
  },
  {
      "name": "Cocos (Keeling) Islands",
      "dial": "61",
      "code": "AUS"
  },
  {
      "name": "Colombia",
      "dial": "57",
      "code": "CO"
  },
  {
      "name": "Comoros",
      "dial": "269",
      "code": "KM"
  },
  {
      "name": "Congo - Brazzaville",
      "dial": "242",
      "code": "RCB"
  },
  {
      "name": "Cook Islands",
      "dial": "682",
      "code": "NZ"
  },
  {
      "name": "Costa Rica",
      "dial": "506",
      "code": "CR"
  },
  {
      "name": "Croatia",
      "dial": "385",
      "code": "HR"
  },
  {
      "name": "Cuba",
      "dial": "53",
      "code": "C"
  },
  {
      "name": "Curaçao",
      "dial": "599",
      "code": null
  },
  {
      "name": "Cyprus",
      "dial": "357",
      "code": "CY"
  },
  {
      "name": "Czechia",
      "dial": "420",
      "code": "CZ"
  },
  {
      "name": "Côte d’Ivoire",
      "dial": "225",
      "code": "CI"
  },
  {
      "name": "North Korea",
      "dial": "850",
      "code": null
  },
  {
      "name": "Congo - Kinshasa",
      "dial": "243",
      "code": "ZRE"
  },
  {
      "name": "Denmark",
      "dial": "45",
      "code": "DK"
  },
  {
      "name": "Djibouti",
      "dial": "253",
      "code": "F"
  },
  {
      "name": "Dominica",
      "dial": "1-767",
      "code": "WD"
  },
  {
      "name": "Dominican Republic",
      "dial": "1-809,1-829,1-849",
      "code": "DOM"
  },
  {
      "name": "Ecuador",
      "dial": "593",
      "code": "EC"
  },
  {
      "name": "Egypt",
      "dial": "20",
      "code": "ET"
  },
  {
      "name": "El Salvador",
      "dial": "503",
      "code": "ES"
  },
  {
      "name": "Equatorial Guinea",
      "dial": "240",
      "code": "EQ"
  },
  {
      "name": "Eritrea",
      "dial": "291",
      "code": "ER"
  },
  {
      "name": "Estonia",
      "dial": "372",
      "code": "EST"
  },
  {
      "name": "Eswatini",
      "dial": "268",
      "code": "SD"
  },
  {
      "name": "Ethiopia",
      "dial": "251",
      "code": "ETH"
  },
  {
      "name": "Falkland Islands",
      "dial": "500",
      "code": null
  },
  {
      "name": "Faroe Islands",
      "dial": "298",
      "code": "FO"
  },
  {
      "name": "Fiji",
      "dial": "679",
      "code": "FJI"
  },
  {
      "name": "Finland",
      "dial": "358",
      "code": "FIN"
  },
  {
      "name": "France",
      "dial": "33",
      "code": "F"
  },
  {
      "name": "French Guiana",
      "dial": "594",
      "code": "F"
  },
  {
      "name": "French Polynesia",
      "dial": "689",
      "code": "F"
  },
  {
      "name": "French Southern Territories",
      "dial": "262",
      "code": "F"
  },
  {
      "name": "Gabon",
      "dial": "241",
      "code": "G"
  },
  {
      "name": "Gambia",
      "dial": "220",
      "code": "WAG"
  },
  {
      "name": "Georgia",
      "dial": "995",
      "code": "GE"
  },
  {
      "name": "Germany",
      "dial": "49",
      "code": "D"
  },
  {
      "name": "Ghana",
      "dial": "233",
      "code": "GH"
  },
  {
      "name": "Gibraltar",
      "dial": "350",
      "code": "GBZ"
  },
  {
      "name": "Greece",
      "dial": "30",
      "code": "GR"
  },
  {
      "name": "Greenland",
      "dial": "299",
      "code": "DK"
  },
  {
      "name": "Grenada",
      "dial": "1-473",
      "code": "WG"
  },
  {
      "name": "Guadeloupe",
      "dial": "590",
      "code": "F"
  },
  {
      "name": "Guam",
      "dial": "1-671",
      "code": "USA"
  },
  {
      "name": "Guatemala",
      "dial": "502",
      "code": "GCA"
  },
  {
      "name": "Guernsey",
      "dial": "44",
      "code": "GBG"
  },
  {
      "name": "Guinea",
      "dial": "224",
      "code": "RG"
  },
  {
      "name": "Guinea-Bissau",
      "dial": "245",
      "code": "GW"
  },
  {
      "name": "Guyana",
      "dial": "592",
      "code": "GUY"
  },
  {
      "name": "Haiti",
      "dial": "509",
      "code": "RH"
  },
  {
      "name": "Heard & McDonald Islands",
      "dial": "672",
      "code": "AUS"
  },
  {
      "name": "Vatican City",
      "dial": "39-06",
      "code": "V"
  },
  {
      "name": "Honduras",
      "dial": "504",
      "code": null
  },
  {
      "name": "Hungary",
      "dial": "36",
      "code": "H"
  },
  {
      "name": "Iceland",
      "dial": "354",
      "code": "IS"
  },
  {
      "name": "India",
      "dial": "91",
      "code": "IND"
  },
  {
      "name": "Indonesia",
      "dial": "62",
      "code": "RI"
  },
  {
      "name": "Iran",
      "dial": "98",
      "code": "IR"
  },
  {
      "name": "Iraq",
      "dial": "964",
      "code": "IRQ"
  },
  {
      "name": "Ireland",
      "dial": "353",
      "code": "IRL"
  },
  {
      "name": "Isle of Man",
      "dial": "44",
      "code": "GBM"
  },
  {
      "name": "Israel",
      "dial": "972",
      "code": "IL"
  },
  {
      "name": "Italy",
      "dial": "39",
      "code": "I"
  },
  {
      "name": "Jamaica",
      "dial": "1-876",
      "code": "JA"
  },
  {
      "name": "Japan",
      "dial": "81",
      "code": "J"
  },
  {
      "name": "Jersey",
      "dial": "44",
      "code": "GBJ"
  },
  {
      "name": "Jordan",
      "dial": "962",
      "code": "HKJ"
  },
  {
      "name": "Kazakhstan",
      "dial": "7",
      "code": "KZ"
  },
  {
      "name": "Kenya",
      "dial": "254",
      "code": "EAK"
  },
  {
      "name": "Kiribati",
      "dial": "686",
      "code": null
  },
  {
      "name": "Kuwait",
      "dial": "965",
      "code": "KWT"
  },
  {
      "name": "Kyrgyzstan",
      "dial": "996",
      "code": "KS"
  },
  {
      "name": "Laos",
      "dial": "856",
      "code": "LAO"
  },
  {
      "name": "Latvia",
      "dial": "371",
      "code": "LV"
  },
  {
      "name": "Lebanon",
      "dial": "961",
      "code": "RL"
  },
  {
      "name": "Lesotho",
      "dial": "266",
      "code": "LS"
  },
  {
      "name": "Liberia",
      "dial": "231",
      "code": "LB"
  },
  {
      "name": "Libya",
      "dial": "218",
      "code": "LAR"
  },
  {
      "name": "Liechtenstein",
      "dial": "423",
      "code": "FL"
  },
  {
      "name": "Lithuania",
      "dial": "370",
      "code": "LT"
  },
  {
      "name": "Luxembourg",
      "dial": "352",
      "code": "L"
  },
  {
      "name": "Madagascar",
      "dial": "261",
      "code": "RM"
  },
  {
      "name": "Malawi",
      "dial": "265",
      "code": "MW"
  },
  {
      "name": "Malaysia",
      "dial": "60",
      "code": "MAL"
  },
  {
      "name": "Maldives",
      "dial": "960",
      "code": "MV"
  },
  {
      "name": "Mali",
      "dial": "223",
      "code": "RMM"
  },
  {
      "name": "Malta",
      "dial": "356",
      "code": "M"
  },
  {
      "name": "Marshall Islands",
      "dial": "692",
      "code": null
  },
  {
      "name": "Martinique",
      "dial": "596",
      "code": "F"
  },
  {
      "name": "Mauritania",
      "dial": "222",
      "code": "RIM"
  },
  {
      "name": "Mauritius",
      "dial": "230",
      "code": "MS"
  },
  {
      "name": "Mayotte",
      "dial": "262",
      "code": null
  },
  {
      "name": "Mexico",
      "dial": "52",
      "code": "MEX"
  },
  {
      "name": "Micronesia",
      "dial": "691",
      "code": null
  },
  {
      "name": "Monaco",
      "dial": "377",
      "code": "MC"
  },
  {
      "name": "Mongolia",
      "dial": "976",
      "code": "MGL"
  },
  {
      "name": "Montenegro",
      "dial": "382",
      "code": "MNE"
  },
  {
      "name": "Montserrat",
      "dial": "1-664",
      "code": null
  },
  {
      "name": "Morocco",
      "dial": "212",
      "code": "MA"
  },
  {
      "name": "Mozambique",
      "dial": "258",
      "code": "MOC"
  },
  {
      "name": "Myanmar",
      "dial": "95",
      "code": "BUR"
  },
  {
      "name": "Namibia",
      "dial": "264",
      "code": "NAM"
  },
  {
      "name": "Nauru",
      "dial": "674",
      "code": "NAU"
  },
  {
      "name": "Nepal",
      "dial": "977",
      "code": "NEP"
  },
  {
      "name": "Netherlands",
      "dial": "31",
      "code": "NL"
  },
  {
      "name": "New Caledonia",
      "dial": "687",
      "code": "F"
  },
  {
      "name": "New Zealand",
      "dial": "64",
      "code": "NZ"
  },
  {
      "name": "Nicaragua",
      "dial": "505",
      "code": "NIC"
  },
  {
      "name": "Niger",
      "dial": "227",
      "code": "RN"
  },
  {
      "name": "Nigeria",
      "dial": "234",
      "code": "WAN"
  },
  {
      "name": "Niue",
      "dial": "683",
      "code": "NZ"
  },
  {
      "name": "Norfolk Island",
      "dial": "672",
      "code": "AUS"
  },
  {
      "name": "Northern Mariana Islands",
      "dial": "1-670",
      "code": "USA"
  },
  {
      "name": "Norway",
      "dial": "47",
      "code": "N"
  },
  {
      "name": "Oman",
      "dial": "968",
      "code": null
  },
  {
      "name": "Pakistan",
      "dial": "92",
      "code": "PK"
  },
  {
      "name": "Palau",
      "dial": "680",
      "code": null
  },
  {
      "name": "Panama",
      "dial": "507",
      "code": "PA"
  },
  {
      "name": "Papua New Guinea",
      "dial": "675",
      "code": "PNG"
  },
  {
      "name": "Paraguay",
      "dial": "595",
      "code": "PY"
  },
  {
      "name": "Peru",
      "dial": "51",
      "code": "PE"
  },
  {
      "name": "Philippines",
      "dial": "63",
      "code": "RP"
  },
  {
      "name": "Pitcairn Islands",
      "dial": "870",
      "code": null
  },
  {
      "name": "Poland",
      "dial": "48",
      "code": "PL"
  },
  {
      "name": "Portugal",
      "dial": "351",
      "code": "P"
  },
  {
      "name": "Puerto Rico",
      "dial": "1",
      "code": "USA"
  },
  {
      "name": "Qatar",
      "dial": "974",
      "code": "Q"
  },
  {
      "name": "South Korea",
      "dial": "82",
      "code": "ROK"
  },
  {
      "name": "Moldova",
      "dial": "373",
      "code": "MD"
  },
  {
      "name": "Romania",
      "dial": "40",
      "code": "RO"
  },
  {
      "name": "Russia",
      "dial": "7",
      "code": "RUS"
  },
  {
      "name": "Rwanda",
      "dial": "250",
      "code": "RWA"
  },
  {
      "name": "Réunion",
      "dial": "262",
      "code": "F"
  },
  {
      "name": "St. Barthélemy",
      "dial": "590",
      "code": null
  },
  {
      "name": "St. Helena",
      "dial": "290",
      "code": "SH"
  },
  {
      "name": "St. Kitts & Nevis",
      "dial": "1-869",
      "code": "KN"
  },
  {
      "name": "St. Lucia",
      "dial": "1-758",
      "code": "WL"
  },
  {
      "name": "St. Martin",
      "dial": "590",
      "code": null
  },
  {
      "name": "St. Pierre & Miquelon",
      "dial": "508",
      "code": "F"
  },
  {
      "name": "St. Vincent & Grenadines",
      "dial": "1-784",
      "code": "WV"
  },
  {
      "name": "Samoa",
      "dial": "685",
      "code": "WS"
  },
  {
      "name": "San Marino",
      "dial": "378",
      "code": "RSM"
  },
  {
      "name": "São Tomé & Príncipe",
      "dial": "239",
      "code": "ST"
  },
  {
      "name": "Saudi Arabia",
      "dial": "966",
      "code": "SA"
  },
  {
      "name": "Senegal",
      "dial": "221",
      "code": "SN"
  },
  {
      "name": "Serbia",
      "dial": "381",
      "code": "SRB"
  },
  {
      "name": "Seychelles",
      "dial": "248",
      "code": "SY"
  },
  {
      "name": "Sierra Leone",
      "dial": "232",
      "code": "WAL"
  },
  {
      "name": "Singapore",
      "dial": "65",
      "code": "SGP"
  },
  {
      "name": "Sint Maarten",
      "dial": "1-721",
      "code": null
  },
  {
      "name": "Slovakia",
      "dial": "421",
      "code": "SK"
  },
  {
      "name": "Slovenia",
      "dial": "386",
      "code": "SLO"
  },
  {
      "name": "Solomon Islands",
      "dial": "677",
      "code": "SB"
  },
  {
      "name": "Somalia",
      "dial": "252",
      "code": "SO"
  },
  {
      "name": "South Africa",
      "dial": "27",
      "code": "ZA"
  },
  {
      "name": "South Georgia & South Sandwich Islands",
      "dial": "500",
      "code": null
  },
  {
      "name": "South Sudan",
      "dial": "211",
      "code": null
  },
  {
      "name": "Spain",
      "dial": "34",
      "code": "E"
  },
  {
      "name": "Sri Lanka",
      "dial": "94",
      "code": "CL"
  },
  {
      "name": "Palestine",
      "dial": "970",
      "code": null
  },
  {
      "name": "Sudan",
      "dial": "249",
      "code": "SUD"
  },
  {
      "name": "Suriname",
      "dial": "597",
      "code": "SME"
  },
  {
      "name": "Svalbard & Jan Mayen",
      "dial": "47",
      "code": null
  },
  {
      "name": "Sweden",
      "dial": "46",
      "code": "S"
  },
  {
      "name": "Switzerland",
      "dial": "41",
      "code": "CH"
  },
  {
      "name": "Syria",
      "dial": "963",
      "code": "SYR"
  },
  {
      "name": "Tajikistan",
      "dial": "992",
      "code": "TJ"
  },
  {
      "name": "Thailand",
      "dial": "66",
      "code": "T"
  },
  {
      "name": "North Macedonia",
      "dial": "389",
      "code": "MK"
  },
  {
      "name": "Timor-Leste",
      "dial": "670",
      "code": "RI"
  },
  {
      "name": "Togo",
      "dial": "228",
      "code": "TG"
  },
  {
      "name": "Tokelau",
      "dial": "690",
      "code": "NZ"
  },
  {
      "name": "Tonga",
      "dial": "676",
      "code": "TO"
  },
  {
      "name": "Trinidad & Tobago",
      "dial": "1-868",
      "code": "TT"
  },
  {
      "name": "Tunisia",
      "dial": "216",
      "code": "TN"
  },
  {
      "name": "Turkey",
      "dial": "90",
      "code": "TR"
  },
  {
      "name": "Turkmenistan",
      "dial": "993",
      "code": "TM"
  },
  {
      "name": "Turks & Caicos Islands",
      "dial": "1-649",
      "code": null
  },
  {
      "name": "Tuvalu",
      "dial": "688",
      "code": "TV"
  },
  {
      "name": "Uganda",
      "dial": "256",
      "code": "EAU"
  },
  {
      "name": "Ukraine",
      "dial": "380",
      "code": "UA"
  },
  {
      "name": "United Arab Emirates",
      "dial": "971",
      "code": null
  },
  {
      "name": "UK",
      "dial": "44",
      "code": "GB"
  },
  {
      "name": "Tanzania",
      "dial": "255",
      "code": "EAT"
  },
  {
      "name": "U.S. Virgin Islands",
      "dial": "1-340",
      "code": "USA"
  },
  {
      "name": "US",
      "dial": "1",
      "code": "USA"
  },
  {
      "name": "Uruguay",
      "dial": "598",
      "code": "ROU"
  },
  {
      "name": "Uzbekistan",
      "dial": "998",
      "code": "UZ"
  },
  {
      "name": "Vanuatu",
      "dial": "678",
      "code": "VU"
  },
  {
      "name": "Venezuela",
      "dial": "58",
      "code": "YV"
  },
  {
      "name": "Vietnam",
      "dial": "84",
      "code": "VN"
  },
  {
      "name": "Wallis & Futuna",
      "dial": "681",
      "code": "F"
  },
  {
      "name": "Western Sahara",
      "dial": "212",
      "code": null
  },
  {
      "name": "Yemen",
      "dial": "967",
      "code": "YAR"
  },
  {
      "name": "Zambia",
      "dial": "260",
      "code": "Z"
  },
  {
      "name": "Zimbabwe",
      "dial": "263",
      "code": "ZW"
  },
  {
      "name": "Åland Islands",
      "dial": "358",
      "code": "FIN"
  }
]