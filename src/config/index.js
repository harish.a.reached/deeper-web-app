import development from './development'
import production from './production'

let environmentName = process.env.NODE_ENV || 'development'

let config

if(environmentName === 'production') {
  config = production
} else {
  config = development
}

export default config
