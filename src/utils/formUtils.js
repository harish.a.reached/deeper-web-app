
import axios from 'axios'
import config from '../config'


async  function getAllLanguages(){
  let options = []
  await axios.get(config.API_ENDPOINT + '/getAllLanguages')
    .then((res) => {
      options = res.data.responseData || [];
    })
    .catch((err) => {})
  return options;
}

async  function getAllAgeGroups(){
  let options = []
  await axios.get(config.API_ENDPOINT + '/getAllAgeGroups')
    .then((res) => {
      options = res.data.responseData || [];
      options = options.map((option) => {
        option.label = `${option.lowerAgeLimit} - ${option.upperAgeLimit}`
        option.value = option.agerGroupId
        return option
      })
    })
    .catch((err) => {})
  return options;
}

async  function getAllCountries(){
  let options = []
  await axios.get(config.API_ENDPOINT + '/getAllCountries')
    .then((res) => {
      options = res.data.responseData || [];
    })
    .catch((err) => {})
  return options;
}

export {
  getAllLanguages,
  getAllAgeGroups,
  getAllCountries
}
