import React from 'react'
import {
  BrowserRouter as Router,
  Link,
  Route,
  Switch,
  Redirect
} from 'react-router-dom'
import FormsHandler from './components/FormsHandler'
import { Container } from 'react-bootstrap'

import history from './history'
import NotFoundPage from './components/NotFoundPage';
import Login from './components/Login';
import HomePage from './components/HomePage';

const App = () => {
  return (
    <div>
      <Container fluid="md">
        <div className="app">
          <Router history={history}>
            <Switch>
              <Route exact path="/login">
                <Login />
              </Route>
              <Route exact path="/forms/:formId">
                <FormsHandler />
              </Route>
              <Route exact path="/">
                <HomePage />
              </Route>
              <Route exact path="page-not-found" component={NotFoundPage} />
              <Route component={NotFoundPage} />
            </Switch>
          </Router>
        </div>
      </Container>
    </div>
  )
}

export default App
