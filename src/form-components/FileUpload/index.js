import React from 'react'

const styles = {
  buttonStyles: {
    width: '100%',
    backgroundColor: '#2A3B57',
    color: 'white',
    // marginLeft: 10,
    // display: 'none'
  },
  labelStyles: {
    width: '100%',
    backgroundColor: '#2A3B57',
    color: 'white',
    // marginLeft: 10,
    textAlign: 'center',
    marginTop: 10,
    padding: 5,
    borderRadius: 5
  },
  fileNameStyles: {
    // marginLeft: 10,
    textAlign: 'center',
    padding: 5,
    color: '#2A3B57'
  }
}

class FileUpload extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      fileChosen: false
    }
    // this.componentDidMount = this.componentDidMount.bind(this)
    this.addEventListener = this.addEventListener.bind(this)
    this.addEventListener()
  }

  addEventListener = () => {
    let actualButton = document.getElementById(this.props.keyName + '_id')
    if(!actualButton) {
      return
    }
    actualButton.addEventListener('change', (event) => {
      let fileContent = event.target.files?.length ? event.target.files[0] : null
      this.setState({
        ...this.state,
        fileChosen: fileContent?.name || null
      })
      this.props.updateFileSelected(this.props.keyName, fileContent)
    })
    
  }

  componentDidMount() {
    this.addEventListener()
  }

  render() {
    return (
      <div style={{marginLeft:10}}>
        <input accept={this.props.accept} id={this.props.keyName + '_id'} type="file" style={styles.buttonStyles} hidden={true}/>
        <div style={styles.fileNameStyles}>{ this.state.fileChosen || 'No file chosen'}</div>
        <label htmlFor={this.props.keyName + '_id'} className="file-upload-label" style={styles.labelStyles}>
          {this.props.label}
        </label>
      </div>
    )
  }

}

export default FileUpload
