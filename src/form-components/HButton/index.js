import React from 'react';
import Button from '@material-ui/core/Button'

const styles = {
  backgroundColor: '#2A3B57',
  color: 'white',
  marginLeft: 10
}

class HButton extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Button disabled={this.props.disabled} style={styles} onClick={() => this.props.onSubmit()}>
        {this.props.buttonValue}
      </Button>
    )
  }
}

export default HButton;
