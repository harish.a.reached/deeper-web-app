
import { TextField } from '@material-ui/core';
import React from 'react';

// import '../HInput/HInput.css'//'./HInput.css';

export default class HInput extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="field-input" style={{padding: 10}}>
        <TextField
          label={this.props.label}
          variant="outlined"
          size="small"
          type={this.props.type || 'text'}
          style={{width: '100%'}}
          onChange={(e)=> this.props.onChange(this.props.keyName, e.target.value)}
          multiline={this.props.multiline ? true: false}
          rows={this.props.rows || 1}
        />
      </div>
    )
  }
}
