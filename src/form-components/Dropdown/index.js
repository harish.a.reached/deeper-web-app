import React from 'react'
import { Select, MenuItem, FormControl, InputLabel } from '@material-ui/core'

const classes = {
  formControl: {
    // margin: theme.spacing(1),
    minWidth: '100%'
  },
  selectEmpty: {
    // marginTop: theme.spacing(2),
  }
}

export default class Dropdown extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      classes
    }
  }

  render() {
    return (
      <div style={{padding: 10, minWidth: 120}}>
        <FormControl variant="outlined" size="small" style={this.state.classes.formControl}>
    <InputLabel id="demo-simple-select-filled-label">{this.props.label}</InputLabel>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            label={this.props.label}
            onChange={(e)=> this.props.onChange(this.props.keyName, e.target.value)}
            value={this.props.value ? this.props.value : ''}
          >
            {
              this.props.options && this.props.options.map((option, optionIndex) => {
                return (
                  <MenuItem key={optionIndex} value={option[this.props.optionValue || 'value']}>
                    {option[this.props.optionLabel || 'label']}
                  </MenuItem>
                )
              })
            }
          </Select>
        </FormControl>
      </div>
    )
  }

}
