const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const { GenerateSW } = require('workbox-webpack-plugin')

module.exports = {
  mode: "production",
  // devtool: 'source-map',
  entry: './index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
    // publicPath: '/'
    // clean: true,
  },
  plugins: [
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      inject: true,
      // title: 'Production',
      template: path.resolve(__dirname, 'public', 'index.html')
    }),
    new GenerateSW({
      clientsClaim: true,
      navigateFallback: path.resolve(__dirname, '/dist/index.html'),
      navigateFallbackAllowlist: [
        new RegExp('^/*'),
        new RegExp('/[^/?]+\\.[^/]+$')
      ]
    })
  ],
  devServer: {
    historyApiFallback: true
  },
  resolve: {
    extensions: [".js", ".jsx", ".json"]
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
      // {
      //   test: /\.(png|svg|jpg|jpeg|gif)$/i,
      //   type: 'asset/resource',
      // },
      {
        test: /\.(jpg|png)$/,
        use: {
          loader: "file-loader",
        }
      },
    ]
  }
};